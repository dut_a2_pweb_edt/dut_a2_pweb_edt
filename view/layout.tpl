<!DOCTYPE html>
<html lang="fr">
	<head >
		<title>EDT</title>
		<meta charset='utf-8'>
		<script src="./bootstrap/jquery/jquery-3.3.1.min.js"></script>
		<script src="./bootstrap/popper/popper.min.js"></script>
		<link rel='stylesheet' href='./bootstrap/css/bootstrap.min.css'>
		<script src='./bootstrap/js/bootstrap.bundle.min.js'></script>
		<link rel="icon" href="./resources/head_icon.png">
		<link rel='stylesheet' href='./view/style/all.css'>
	</head>
	<?php require $chemin?>